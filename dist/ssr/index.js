module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("firebase-admin");

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(3);


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 *
 * WARNING!
 * If you import anything from node_modules, then make sure that the package is specified
 * in package.json > dependencies and NOT in devDependencies
 *
 * Note: This file is used only for PRODUCTION. It is not picked up while in dev mode.
 *   If you are looking to add common DEV & PROD logic to the express app, then use
 *   "src-ssr/extension.js"
 */

const express = __webpack_require__(0)
const compression = __webpack_require__(4)

const ssr = __webpack_require__(5)
const extension = __webpack_require__(12)
const app = express()
const port = process.env.PORT || 3000

const serve = (path, cache) => express.static(ssr.resolveWWW(path), {
  maxAge: cache ? 1000 * 60 * 60 * 24 * 30 : 0
})

// gzip
app.use(compression({ threshold: 0 }))

// serve this with no cache, if built with PWA:
if (ssr.settings.pwa) {
  app.use(ssr.resolveUrl('/service-worker.js'), serve('service-worker.js'))
}

// serve "www" folder
app.use(ssr.resolveUrl('/'), serve('.', true))

// we extend the custom common dev & prod parts here
extension.extendApp({ app, ssr })

// this should be last get(), rendering with SSR
app.get(ssr.resolveUrl('*'), (req, res) => {
  res.setHeader('Content-Type', 'text/html')

  // SECURITY HEADERS
  // read more about headers here: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
  // the following headers help protect your site from common XSS attacks in browsers that respect headers
  // you will probably want to use .env variables to drop in appropriate URLs below,
  // and potentially look here for inspiration:
  // https://ponyfoo.com/articles/content-security-policy-in-express-apps

  // https://developer.mozilla.org/en-us/docs/Web/HTTP/Headers/X-Frame-Options
  // res.setHeader('X-frame-options', 'SAMEORIGIN') // one of DENY | SAMEORIGIN | ALLOW-FROM https://example.com

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
  // res.setHeader('X-XSS-Protection', 1)

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
  // res.setHeader('X-Content-Type-Options', 'nosniff')

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
  // res.setHeader('Access-Control-Allow-Origin', '*') // one of '*', '<origin>' where origin is one SINGLE origin

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-DNS-Prefetch-Control
  // res.setHeader('X-DNS-Prefetch-Control', 'off') // may be slower, but stops some leaks

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
  // res.setHeader('Content-Security-Policy', 'default-src https:')

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/sandbox
  // res.setHeader('Content-Security-Policy', 'sandbox') // this will lockdown your server!!!
  // here are a few that you might like to consider adding to your CSP
  // object-src, media-src, script-src, frame-src, unsafe-inline

  ssr.renderToString({ req, res }, (err, html) => {
    if (err) {
      if (err.url) {
        if (err.code) res.redirect(err.code, err.url)
        else res.redirect(err.url)
      } else if (err.code === 404) {
        // Should reach here only if no "catch-all" route
        // is defined in /src/routes
        res.status(404).send('404 | Page Not Found')
      } else {
        // Render Error Page or
        // create a route (/src/routes) for an error page and redirect to it
        res.status(500).send('500 | Internal Server Error')
        if (ssr.settings.debug) {
          console.error(`500 on ${req.url}`)
          console.error(err)
          console.error(err.stack)
        }
      }
    }
    else {
      res.send(html)
    }
  })
})

app.listen(port, () => {
  console.log(`Server listening at port ${port}`)
})


/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("compression");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * THIS FILE IS GENERATED AUTOMATICALLY.
 * DO NOT EDIT.
 **/

const fs = __webpack_require__(6)
const path = __webpack_require__(7)
const LRU = __webpack_require__(8)
const { createBundleRenderer } = __webpack_require__(9)

const resolve = file => path.join(__dirname, file)
const template = fs.readFileSync(resolve('template.html'), 'utf-8')
const bundle = __webpack_require__(10)
const clientManifest = __webpack_require__(11)

const settings = {
  "pwa": false,
  "manualHydration": false,
  "componentCache": {
    "max": 1000,
    "maxAge": 900000
  },
  "debug": false,
  "publicPath": "/"
}

const doubleSlashRE = /\/\//
const { publicPath } = settings


if (process.env.DEBUG) {
  settings.debug = true
}

const rendererOptions = {
  template,
  clientManifest,
  // for component caching
  cache: new LRU(settings.componentCache),
  basedir: __dirname,
  // recommended for performance
  runInNewContext: false
}

const resolveUrl = url => url ? (publicPath + url).replace(doubleSlashRE, '/') : publicPath

// https://ssr.vuejs.org/api/#renderer-options
// https://github.com/vuejs/vue/blob/dev/packages/vue-server-renderer/README.md#why-use-bundlerenderer
let renderer = createBundleRenderer(bundle, rendererOptions)

module.exports.resolveUrl = resolveUrl

module.exports.renderToString = function (opts, cb) {
  const ctx = {
    ...opts,
    url: opts.req.url
  }


  renderer.renderToString(ctx, (err, html) => {
    cb(err, err ? html : ctx.$getMetaHTML(html, ctx))
  })

}

module.exports.resolveWWW = function (file) {
  return resolve('www/' + file)
}

module.exports.mergeRendererOptions = function (opts) {
  renderer = createBundleRenderer(
    bundle,
    Object.assign(rendererOptions, opts)
  )
}

module.exports.settings = settings


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("lru-cache");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("vue-server-renderer");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("./quasar.server-manifest.json");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("./quasar.client-manifest.json");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 *
 * WARNING!
 * If you import anything from node_modules, then make sure that the package is specified
 * in package.json > dependencies and NOT in devDependencies
 *
 * Note: This file is used for both PRODUCTION & DEVELOPMENT.
 * Note: Changes to this file (but not any file it imports!) are picked up by the
 * development server, but such updates are costly since the dev-server needs a reboot.
 */
const express = __webpack_require__(0)
// const ProductRoute = require('./routes/product')
const Route = __webpack_require__(13)

const Admin = __webpack_require__(1)
const serviceAccount = __webpack_require__(16)
Admin.initializeApp({
  credential: Admin.credential.cert(serviceAccount),
  databaseURL: 'https://petepokerworld-17526-default-rtdb.firebaseio.com'
})

module.exports.extendApp = function ({ app, ssr }) {
  /*
     Extend the parts of the express app that you
     want to use with development server too.

     Example: app.use(), app.get() etc
  */
  app.use(express.json())
  app.use('/api', Route)

  app.use((err, req, res, next) => {
    err.statusCode = err.statusCode || 500 // if no statusCode is defined, then use HTTP500
    err.status = err.status || 'error'
    console.log(err)
    // return error status and message to the requester
    res.status(err.statusCode).json({
      code: err.code,
      msg: err.message
    })
  })
}


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

const { Router } = __webpack_require__(0)
const router = Router()
const Axios = __webpack_require__(14)
const Admin = __webpack_require__(1)
const AuthMiddleware = __webpack_require__(15)

const Request = Axios.create({
  baseURL: 'https://mpwei-product-api-kxnw4gb5uq-de.a.run.app',
  timeout: 60000,
  headers: {
    'Content-Type': 'application/json'
  }
})

router.get('/', function (req, res, next) {
  return res.send({
    msg: 'success',
    code: 200
  })
})

router.post('/', function (req, res, next) {
  return Request({
    method: 'post',
    url: req.body.URL,
    data: {
      Project: 'PPW',
      ...req.body.Data
    }
  }).then(({ data }) => {
    return res.send(data)
  }).catch((error) => {
    return next(error)
  })
})

router.post('/app/range-viewer', function (req, res, next) {
  return Admin.auth().verifyIdToken(req.headers['id-token'], true).then((decodedToken) => {
    return Admin.auth().createCustomToken(decodedToken.uid).then((customToken) => {
      return res.send({
        msg: 'success',
        token: customToken,
        code: 200
      })
    }).catch((error) => {
      return next(error)
    })
  }).catch((error) => {
    return next(error)
  })
})

router.post('/getUser', AuthMiddleware, function (req, res, next) {
  return Admin.auth().getUser(req.body.uid).then((userRecord) => {
    return res.send({
      msg: 'success',
      user: userRecord.toJSON(),
      code: 200
    })
  }).catch((error) => {
    return next(error)
  })
})

router.post('/getUserInfo', AuthMiddleware, function (req, res, next) {
  return Admin.firestore().collection('MemberData').doc(req.body.uid).get().then((doc) => {
    return res.send({
      msg: 'success',
      userData: (doc.exists ? doc.data() : {}),
      code: 200
    })
  }).catch((error) => {
    return next(error)
  })
})

module.exports = router


/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

const Admin = __webpack_require__(1)

module.exports = function (req, res, next) {
  return Admin.auth().verifyIdToken(req.headers['id-token'], true).then((decodedToken) => {
    req.body.uid = decodedToken.uid
    next()
  }).catch((error) => {
    console.log(JSON.stringify(error))
    if (error.code === 'auth/id-token-revoked') {
      console.log('Token has been revoked.')
    } else {
      console.log('Token is invalid.')
    }
    next(error)
  })
}


/***/ }),
/* 16 */
/***/ (function(module) {

module.exports = JSON.parse("{\"type\":\"service_account\",\"project_id\":\"petepokerworld-17526\",\"private_key_id\":\"a5e4ca25f3d9e4b81f182951dd6446207f86e438\",\"private_key\":\"-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCztwIioEj8Gef1\\nF0upm9YD91fnyJtMqN215X4uhkeKu1qrjRU+QKXNf2/syL7GJP6f1oXVWHr7v/GW\\njUJfC1k+TQAqYKMyesYdXWPTztQKlnO2L0n3uvJFOSvJ6e72lxgJHUk3Rz6vFPhs\\nuBPB/3KS14Ko9lXgG+azR1xsIh+WxUA6woGdKC6YtGvF/veTi5t4fBG/v4o4TOgZ\\n0hWvNhANMCDHeFBs8+1V691jKDozOS61nWHJq/MWFTNaTvoTam/e7kxkZemPA2SB\\nmtMTB/xY/JxugePLM/XWp53s7cXWu0W3I89XlKILQ8tDrmDMjjby8wHsfiRB01Fw\\n2cVfVtG7AgMBAAECggEABpyF9JYm+TdOxEJ+Ixg7kFvKHRnRNMnFlkqCEN4Zxa3M\\n0SDYlji0oNG+YVylQA7eWyz+MXIW3CocjihWN77oIpWcmvKBLns7zJrB8GncfSM5\\n0Ir+0qFLBmbrCHuVsfavCK7OIb16PeCsnvtKpslqAqLxCJw/XoZIP3IxoTzEIrzB\\nIx6WQXrWJ9CS1tbpA2Nkncu63s7ax7aosWIsl2y3pVpfpkHI/xjlXHRWuqVscyHv\\nkKIOYXsviM3bfaQsC0c9nXyfy8HbMQsJ3sfG6rzuuvNTHJWEfZ0aGcDLwi8JiVv0\\nKYSnNi9ZLc+YMwNWwKbllffPXUI6Z/kcne4Sbcbm2QKBgQDnk+hDn2jBU+S1QGJ9\\nelCrzvzNJvgSweLuOUu1xt6Vz4882PKZHu7t1ST5FfU9nKBIaY0lc4TutRcy7o+/\\nirSaHSmQvH2ohprHfPkbOx0VI9myXJIe70OPCgxP8VG5KT+rMQq0KPatI2B1dHIt\\nN/tzrD6H3f2uD+fm+ShGpU3Z7QKBgQDGquqpA3i/6fM2vlaFHA3PJqWHOIDLY/+K\\nYi9PR6iXbsmpyMhDlL8wajFDwkysC3XkboGHPhh4Ao57N9xa+i+9rtfQvsXbbeRt\\nrlhGEehpSl3Wb7ugbOsER5L3335ZTCt9D9VJLNvSKwSRhjt7rXTmpgM6IQU46GNW\\nLsfcZLzFRwKBgB2SfLmN9MGjZfRjaXBem0ilYRENpeY3TjRfqrRm1evJmqAJkElP\\ncvF6I4OPc0bP5oP4vqaGrvYQuAqv/jlWjCOWD8XE3EMWFVPeOK1qihc2AcnOGnbK\\n1RTCErputaHwi4nf9F73UDDh8KJvIVl26axm1YFiLs/4hgOv053Z5CwtAoGBAIPL\\nnVM0l2WRuaTGjrVNkCettsBHDsJfe4Z9bm9E1aZoe+Zj6d9TjaZ7vGT8QEseaz54\\nxPtIiYkMASPjQg8kzIkktc2o0a+pbpIIyEzr0mgPSsNYanuZ5it3pBtGrBZnfq4/\\n07nwxX/ew/AawdlhkBz3Y0qr30AKguQIstQWDFk/AoGBALzBEEXOnV60hwA0sDyp\\nJlE7Z9YOBsmElyDum54Nh+AxITxLZUdJeK0M0axJE2NPEfvsquHuMJACwCDF+lEK\\niU9uADKXAm7gNFmlUEGX0uQTW0sIdHWMn4Xw+AAYG3pWl0EQj/zB51L7eK+jbwoP\\n2dtqDIRj5VnmYlwl0guK1NIO\\n-----END PRIVATE KEY-----\\n\",\"client_email\":\"firebase-adminsdk-bnqh2@petepokerworld-17526.iam.gserviceaccount.com\",\"client_id\":\"110400631156942638084\",\"auth_uri\":\"https://accounts.google.com/o/oauth2/auth\",\"token_uri\":\"https://oauth2.googleapis.com/token\",\"auth_provider_x509_cert_url\":\"https://www.googleapis.com/oauth2/v1/certs\",\"client_x509_cert_url\":\"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-bnqh2%40petepokerworld-17526.iam.gserviceaccount.com\"}");

/***/ })
/******/ ]);