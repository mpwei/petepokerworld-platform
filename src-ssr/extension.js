/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 *
 * WARNING!
 * If you import anything from node_modules, then make sure that the package is specified
 * in package.json > dependencies and NOT in devDependencies
 *
 * Note: This file is used for both PRODUCTION & DEVELOPMENT.
 * Note: Changes to this file (but not any file it imports!) are picked up by the
 * development server, but such updates are costly since the dev-server needs a reboot.
 */
const express = require('express')
// const ProductRoute = require('./routes/product')
const Route = require('./routes')

const Admin = require('firebase-admin')
const serviceAccount = require('./cert/petepokerworld-17526-firebase-adminsdk-bnqh2-a5e4ca25f3.json')
Admin.initializeApp({
  credential: Admin.credential.cert(serviceAccount),
  databaseURL: 'https://petepokerworld-17526-default-rtdb.firebaseio.com'
})

module.exports.extendApp = function ({ app, ssr }) {
  /*
     Extend the parts of the express app that you
     want to use with development server too.

     Example: app.use(), app.get() etc
  */
  app.use(express.json())
  app.use('/api', Route)

  app.use((err, req, res, next) => {
    err.statusCode = err.statusCode || 500 // if no statusCode is defined, then use HTTP500
    err.status = err.status || 'error'
    console.log(err)
    // return error status and message to the requester
    res.status(err.statusCode).json({
      code: err.code,
      msg: err.message
    })
  })
}
