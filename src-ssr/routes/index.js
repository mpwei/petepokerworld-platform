const { Router } = require('express')
const router = Router()
const Axios = require('axios')
const Admin = require('firebase-admin')
const AuthMiddleware = require('../middleware/auth')

const Request = Axios.create({
  baseURL: 'https://mpwei-product-api-kxnw4gb5uq-de.a.run.app',
  timeout: 60000,
  headers: {
    'Content-Type': 'application/json'
  }
})

router.get('/', function (req, res, next) {
  return res.send({
    msg: 'success',
    code: 200
  })
})

router.post('/', function (req, res, next) {
  return Request({
    method: 'post',
    url: req.body.URL,
    data: {
      Project: 'PPW',
      ...req.body.Data
    }
  }).then(({ data }) => {
    return res.send(data)
  }).catch((error) => {
    return next(error)
  })
})

router.post('/app/range-viewer', function (req, res, next) {
  return Admin.auth().verifyIdToken(req.headers['id-token'], true).then((decodedToken) => {
    return Admin.auth().createCustomToken(decodedToken.uid).then((customToken) => {
      return res.send({
        msg: 'success',
        token: customToken,
        code: 200
      })
    }).catch((error) => {
      return next(error)
    })
  }).catch((error) => {
    return next(error)
  })
})

router.post('/getUser', AuthMiddleware, function (req, res, next) {
  return Admin.auth().getUser(req.body.uid).then((userRecord) => {
    return res.send({
      msg: 'success',
      user: userRecord.toJSON(),
      code: 200
    })
  }).catch((error) => {
    return next(error)
  })
})

router.post('/getUserInfo', AuthMiddleware, function (req, res, next) {
  return Admin.firestore().collection('MemberData').doc(req.body.uid).get().then((doc) => {
    return res.send({
      msg: 'success',
      userData: (doc.exists ? doc.data() : {}),
      code: 200
    })
  }).catch((error) => {
    return next(error)
  })
})

module.exports = router
