const Admin = require('firebase-admin')

module.exports = function (req, res, next) {
  return Admin.auth().verifyIdToken(req.headers['id-token'], true).then((decodedToken) => {
    req.body.uid = decodedToken.uid
    next()
  }).catch((error) => {
    console.log(JSON.stringify(error))
    if (error.code === 'auth/id-token-revoked') {
      console.log('Token has been revoked.')
    } else {
      console.log('Token is invalid.')
    }
    next(error)
  })
}
