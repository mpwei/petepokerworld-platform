import EN from './en-us'
import TW from './zh-tw'

export default {
  'en-us': EN,
  'zh-tw': TW
}
