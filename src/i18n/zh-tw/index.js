export default {
  Message: {
    'auth/email-already-exists': '',
    'auth/id-token-expired': '憑證已過期，請重新登入。',
    'auth/id-token-revoked': '憑證已撤銷，請重新登入。',
    'auth/insufficient-permission': '權限不足。',
    'auth/internal-error': '',
    'auth/argument-error': '憑證解密失敗，請重新登入。',
    'auth/invalid-argument': '',
    'auth/invalid-disabled-field': '',
    'auth/invalid-display-name': '',
    'auth/invalid-email': '帳號格式錯誤，須為電子郵件格式。',
    'auth/user-not-found': '此帳號不存在。',
    'auth/too-many-requests': '嘗試次數過多，請稍後再試。',
    'auth/wrong-password': '密碼錯誤，請重新輸入',
    'auth/weak-password': '密碼長度或強度不足',
    'auth/email-already-in-use': '此信箱已註冊過！',
    'auth/operation-not-allowed': '不允許的登入方式'
  },
  Profile: {
    'Profile/Facebook': 'Facebook登入',
    'Profile/Google': 'Google登入',
    'Profile/Binding': '已綁定'
  }
}
