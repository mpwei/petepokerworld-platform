import Vue from 'vue'
import { date } from 'quasar'

Vue.prototype.$DateTime = (TimeStamp) => {
  return date.formatDate(TimeStamp, 'YYYY-MM-DD HH:mm:ss')
}
