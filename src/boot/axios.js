import Vue from 'vue'
import axios from 'axios'

// Vue Instance
Vue.prototype.$axios = axios
let InnerApplicationInterfaceURL
if (process.env.VUE_ENV === 'server') {
  InnerApplicationInterfaceURL = process.env.APPLICATION_INTERFACE_URL
} else {
  InnerApplicationInterfaceURL = location.protocol + '//' + location.host + '/api/'
}

// Inner Request
const InnerRequest = axios.create({
  baseURL: InnerApplicationInterfaceURL,
  timeout: 30000
})

export { axios, InnerRequest }
