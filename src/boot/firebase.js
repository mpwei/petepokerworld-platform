import Firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/storage'

try {
  Firebase.initializeApp({
    apiKey: process.env.VUE_APP_FIREBASE_APIKEY,
    authDomain: process.env.VUE_APP_FIREBASE_AUTHDOMAIN,
    storageBucket: process.env.VUE_APP_FIREBASE_BUCKET,
    projectId: process.env.VUE_APP_FIREBASE_PROJECTID,
    appId: process.env.VUE_APP_FIREBASE_APPID,
    measurementId: process.env.VUE_APP_FIREBASE_MEASUREMENTID,
    databaseURL: process.env.VUE_APP_FIREBASE_DATABASEURL,
    messagingSenderId: process.env.VUE_APP_FIREBASE_SENDERID
  })
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)
  }
}

export const firestore = Firebase.firestore(),
  storage = Firebase.storage(),
  auth = Firebase.auth(),
  database = Firebase.database(),
  app = Firebase

export default ({ store, Vue, router, app }) => {
  // Tell the application what to do when the
  // authentication state has changed
  Firebase.auth().onAuthStateChanged((currentUser) => {
    store.dispatch('UserModule/CheckAuth', currentUser).catch((err) => {
      console.log(err)
      // if (process.env.VUE_ENV !== 'server') {
      //   if (err.message === 'Login/Unauthorized' && router.app.$route.name !== 'Login') {
      //     router.replace({
      //       path: '/'
      //     })
      //   }
      // }
    }).then(() => {
      if (store.getters['UserModule/User']) {
        // 處理導向
        if (router.app.$route.name === 'Login') {
          if (router.app.$route.query.redirect) {
            router.replace(router.app.$route.query.redirect)
          } else {
            router.replace('/')
          }
        }
      }
    })
  })

  Vue.prototype.$Firebase = (_Service) => {
    if (_Service === 'app') {
      return Firebase
    }
    return Firebase[_Service]()
  }
  store.$Firebase = Firebase
}
