import Vue from 'vue'
import { Notify } from 'quasar'

Notify.setDefaults({
  position: 'top-right',
  timeout: 2000,
  textColor: 'white',
  actions: [{ icon: 'close', color: 'white' }]
})

const Notification = (Data) => {
  let color, message, icon
  switch (Data.Type) {
    case 'Success':
      message = '成功'
      color = 'green'
      icon = 'check_circle'
      break
    case 'Info':
      message = '提示'
      color = 'light-blue'
      icon = 'info'
      break
    case 'Error':
      message = '錯誤'
      color = 'red-6'
      icon = 'cancel'
      break
    case 'Warning':
      message = '警告'
      color = 'orange'
      icon = 'report_problem'
      break
  }
  return new Promise((resolve) => {
    Notify.create({
      position: 'top',
      color,
      icon,
      caption: Data.Message,
      message
    })
    resolve()
  })
}

Vue.prototype.$Notify = (Data) => {
  return Notification(Data).then(() => true)
}

export {
  Notification
}
