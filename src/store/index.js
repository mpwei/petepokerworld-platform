import Vue from 'vue'
import Vuex from 'vuex'
import PageModule from './PageModule'
import UserModule from './UserModule'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      UserModule,
      PageModule
    },

    strict: process.env.DEBUGGING
  })

  return Store
}
