import { firestore, app, database, auth } from 'boot/firebase'
import { Notification } from 'boot/notify'
import { InnerRequest } from 'boot/axios'

export default {
  namespaced: true,
  getters: {
    User (state) {
      return state.User
    },
    IsLoggedIn (state) {
      try {
        return state.User.uid !== null
      } catch {
        return false
      }
    }
  },
  mutations: {
    SetAuth (state, value) {
      if (value) {
        state.User = {
          uid: value.uid,
          displayName: value.displayName,
          email: value.email,
          phoneNumber: value.phoneNumber,
          photoURL: value.photoURL,
          providerData: value.providerData,
          metadata: value.metadata,
          refreshToken: value.refreshToken
        }
      } else {
        state.User = null
      }
    },
    SetUserData (state, value) {
      state.UserData = Object.assign({}, value)
    }
  },
  actions: {
    CheckAuth ({ commit }, user) {
      return new Promise((resolve, reject) => {
        if (user !== null) {
          commit('SetAuth', user)
          resolve({
            message: 'Login/Success'
          })
        } else {
          commit('SetAuth', null)
          reject({
            message: 'Login/Unauthorized'
          })
        }
      })
    },
    // 新增使用者資訊
    AddMemberData ({ commit, $Firebase }, user) {
      return firestore.collection('MemberData').doc(user.uid).set({
        Uid: user.uid,
        Name: user.Name || '',
        NickName: user.NickName || '',
        Sex: user.Sex || 'M',
        Email: user.Email,
        Class: user.Class || 'M',
        Birthday: user.Birthday || '',
        Phone: user.Phone || '',
        Mobile: user.Mobile || '',
        Country: user.Country || '',
        City: user.City || '',
        Area: user.Area || '',
        Address: user.Address || '',
        Family: user.Family || [],
        Level: user.Level || 1,
        UpdateTime: app.firestore.FieldValue.serverTimestamp(),
        CreateTime: app.firestore.FieldValue.serverTimestamp(),
        Groups: user.Group || [],
        Tags: user.Tags || [],
        Note: user.Note || '',
        OpenID: {
          Facebook: user.FacebookOpenID || '',
          Line: user.LineOpenID || ''
        },
        Variable: user.Variable || {}
      }).then(() => {
        return true
      }).catch((error) => {
        throw error
      })
    },
    // 更新使用者資訊
    UpdateUserInfo ({ commit }, user) {
      const FieldName = [
        'Name', 'NickName', 'Sex', 'Birthday', 'Phone', 'Mobile', 'Country', 'City', 'Area', 'Address', 'OpenID', 'Variable'
      ]
      const UpdateData = {
        UpdateTime: app.firestore.FieldValue.serverTimestamp()
      }
      FieldName.forEach((field) => {
        if (typeof user[field] !== 'undefined') {
          UpdateData[field] = user[field]
        }
      })
      return firestore.collection('MemberData').doc(user.uid).update(UpdateData).then(() => {
        return true
      }).catch((error) => {
        throw error
      })
    },
    // 取得使用者資訊
    GetUserInfo ({ commit }, idToken) {
      // return firestore.collection('MemberData').doc(user.uid).get().then((doc) => {
      //   if (doc.exists) {
      //     return doc.data()
      //   } else {
      //     return {}
      //   }
      // }).catch((error) => {
      //   throw error
      // })
      return InnerRequest.request({
        method: 'post',
        url: '/getUserInfo',
        headers: {
          'id-token': idToken
        }
      }).then(({ data }) => {
        return data.userData
      }).catch((error) => {
        throw error
      })
    },
    AddFavorites ({ commit, getters, state }, product) {
      if (!getters.IsLoggedIn) {
        Notification({
          Type: 'Error',
          Message: '請先登入會員'
        }).then(() => true)
        return false
      }
      const FirestoreReference = firestore.collection('FavoriteList')
      const DatabaseReference = database.ref(`ProductStatistics/${product.SeriesNum}/Favorites`)
      return Promise.all([
        FirestoreReference.add({
          Uid: state.User.uid,
          Name: product.Name,
          Brand: product.Brand,
          Categories: product.Categories,
          Cover: product.Cover,
          OverViewImages: product.OverViewImages,
          SeriesNum: product.SeriesNum,
          CustomSeriesNum: product.CustomSeriesNum,
          HeaderText: product.HeaderText,
          OtherName: product.OtherName,
          Tags: product.Tags,
          Model: product.Model
        }),
        DatabaseReference.transaction(function (currentValue) {
          return (currentValue || 0) + 1
        })
      ]).then(() => {
        Notification({
          Type: 'Success',
          Message: '成功加入願望清單'
        }).then(() => true)
      }).catch((error) => {
        console.log(JSON.stringify(error))
        Notification({
          Type: 'Error',
          Message: '加入願望清單失敗'
        }).then(() => true)
        throw error
      })
    },
    Logout () {
      return auth.signOut().then(() => {
        Notification({
          Type: 'Success',
          Message: '已登出'
        }).then(() => true)
      })
    }
  },
  state () {
    return {
      User: null,
      UserData: {},
      Permission: {}
    }
  }
}
