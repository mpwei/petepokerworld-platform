import { InnerRequest } from 'boot/axios'

export default {
  namespaced: true,
  getters: {},
  mutations: {
    SetList (state, payloads) {
      state.List = payloads.Data
      state.Pagination = payloads.Pagination
    },
    SetDetail (state, payloads) {
      state.Detail = payloads.Data
    },
    SetCategoryList (state, payloads) {
      state.CategoryListData.List = payloads.Data
      state.CategoryListData.Pagination = payloads.Pagination
    },
    SetCategoryDetail (state, payloads) {
      state.CategoryDetail = payloads.Data
    }
  },
  actions: {
    GetList ({ commit }, payloads) {
      return InnerRequest.request({
        method: 'post',
        url: '/',
        data: {
          URL: payloads.URL,
          Data: payloads.Data
        }
      }).then(({ data }) => {
        if (payloads.CommitToStore) {
          switch (payloads.ListType) {
            case 'Category':
              commit('SetCategoryList', data)
              break
            case 'Single':
            default:
              commit('SetList', data)
              break
          }
        }
        return data
      }).catch((error) => {
        console.log(error)
        throw error
      })
    },
    GetDetail ({ commit }, payloads) {
      return InnerRequest.request({
        method: 'post',
        url: '/',
        data: {
          URL: payloads.URL,
          Data: payloads.Data
        }
      }).then(({ data }) => {
        if (payloads.CommitToStore) {
          switch (payloads.DetailType) {
            case 'Category':
              commit('SetCategoryDetail', data)
              break
            case 'Single':
            default:
              commit('SetDetail', data)
              break
          }
        }
        return data
      }).catch((error) => {
        console.log(error)
        throw error
      })
    }
  },
  state () {
    return {
      Data: null,
      List: [],
      Pagination: {
        Page: 1,
        PerPage: 20,
        TotalItems: 0,
        TotalPages: 0
      },
      Detail: {},
      CategoryDetail: {},
      PageData: {
        List: [],
        Pagination: {
          Page: 1,
          PerPage: 20,
          TotalItems: 0,
          TotalPages: 0
        }
      },
      CategoryListData: {
        List: [],
        Pagination: {
          Page: 1,
          PerPage: 20,
          TotalItems: 0,
          TotalPages: 0
        }
      }
    }
  }
}
