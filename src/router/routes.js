
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', redirect: '/main' },
      { path: 'main', component: () => import('pages/Index.vue'), name: 'Index' },
      { path: 'event/:Tags', component: () => import('pages/Tag.vue'), name: 'Tags' },
      { path: 'search/:Name', component: () => import('pages/Search.vue'), name: 'Search' },
      { path: 'category', component: () => import('pages/Category.vue'), name: 'All' },
      { path: 'category/:Slug', component: () => import('pages/Category.vue'), name: 'Category' },
      { path: 'detail/:SeriesNum', component: () => import('pages/Detail.vue'), name: 'DetailSeriesNum' },
      { path: 'courses/detail/:Slug', component: () => import('pages/Course.vue'), name: 'CourseDetailSlug', meta: { Auth: true } },
      { path: 'courses/detail/:ID', component: () => import('pages/Course.vue'), name: 'CourseDetailID', meta: { Auth: true } },
      // { path: 'courses/list/:Slug', component: () => import('pages/CourseList.vue'), name: 'CourseList' },
      { path: 'article/list/:Slug', component: () => import('pages/ArticleList.vue'), name: 'ArticleList' },
      { path: 'article/detail/:Slug', component: () => import('pages/Article.vue'), name: 'ArticleDetail' }
    ]
  },
  {
    path: '/member',
    name: 'Member',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'profile', component: () => import('pages/member/Profile.vue'), name: 'Profile', meta: { Auth: true } },
      { path: 'transaction', component: () => import('pages/member/Transaction.vue'), name: 'Transaction', meta: { Auth: true } },
      { path: 'subscription', component: () => import('pages/member/Subscription.vue'), name: 'Subscription', meta: { Auth: true } }
    ]
  },
  {
    path: '/member',
    component: () => import('layouts/DefaultLayout.vue'),
    children: [
      { path: 'login', component: () => import('pages/member/Login.vue'), name: 'Login' },
      { path: 'register', component: () => import('pages/member/Register.vue'), name: 'Register' },
      { path: 'rangeviewer', component: () => import('pages/member/RangeViewer.vue'), name: 'RangeViewer', meta: { Auth: true } }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
